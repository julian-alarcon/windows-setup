# Setup Windows Computer

## Supported OS

Windows 11

## Start Script

1. Open a Command/Windows Terminal as Administrator

2. Initialize setup

```cmd
init.cmd
```

## Manual Steps

* Pin Apps to Taskbar: Firefox, Windows Terminal
* Set Default app (related to [hash](https://stackoverflow.com/questions/17946282/whats-the-hash-in-hkcu-software-microsoft-windows-currentversion-explorer-filee))
	* Firefox: .htm, .html, .pdf, .shtml, .svg, .xht, .xhtml, HTTP, HTTPS, MAILTO
	* VSCodium: .txt, .ps1, .sh, .go, .js, .css, .java
* Set Firefox
	* Firefox Sync
	* Default Search
* Set MS Edge
	* Default Search to Google
	* Disable telemetry
* AMD: Settings -> Hotkeys -> Toggle Performance Logging: NONE

## Pending

Manually Update Windows Store Apps
	"App Installer" (winget)
Manually update with winget
	"Valve.Steam"
Disable Windows Telemetry
Install Windows Updates

## Commands

* Update: `winget upgrade --all`
* Get URL link to installer: `winget show THE_APP_ID -s winget`

### Git setup

```cmd
git config --global user.email "alarconj@gmail.com"
git config --global user.name "Julian Alarcon"
git credential-manager configure
git config --global credential.helper wincred
```

## References

* Find apps: [winget.run](https://winget.run) and [winstall.app](https://winstall.app)
