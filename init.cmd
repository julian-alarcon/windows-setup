@echo off
echo "WINDOWS INITIALIZATION STARTED"

echo "##################################################"
echo "Installing latest Powershell..."
winget install --no-upgrade --accept-package-agreements --accept-source-agreements --exact --silent --id Microsoft.PowerShell
echo "Powershell installed!!!"

echo "##################################################"
echo "Installing and removing apps"
pwsh setup-apps.ps1

echo "##################################################"
echo "Updating Applications..."
winget upgrade --silent --accept-package-agreements --accept-source-agreements --all
:: https://github.com/microsoft/winget-cli/issues/2458
echo "Applications updated!!!"

echo "##################################################"
pwsh set-config.ps1

echo "##################################################"
echo "WINDOWS INITIALIZATION FINISHED!"
