# Install Apps with Winget
Write-Host "Installing and removing apps..."
Write-Host "##################################################"
Write-Host "Installing Apps..."
$AppsInstallList = Get-Content -Path apps-install-list.txt

ForEach ($App in $AppsInstallList) {
    Write-Host "Installing: ""$App"""
    winget install --no-upgrade --accept-package-agreements --accept-source-agreements --exact --silent --id "$App"
}

Write-Host "##################################################"
Write-Host "Removing Apps..."

$AppsRemoveList = Get-Content -Path apps-remove-list.txt

ForEach ($App in $AppsRemoveList) {
    Write-Host "Uninstalling: ""$App"""
    winget uninstall --accept-source-agreements --exact --silent --id "$App"
}

# Complete process
Write-Host "Installed and removed apps!!!"
